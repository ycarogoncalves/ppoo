/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapper;

import java.util.ArrayList;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import produto.Produto;

/**
 *
 * @author Ycaro
 */
public interface ProdutoMapper {
    @Insert("INSERT INTO "
    +"produto(nome,descricao,valor,qtde)"+"VALUES(#{nome},#{descricao},#{valor},#{qtde})")
    void insertProduto(Produto produto);
    
    @Select("SELECT * FROM produto ")
    ArrayList<Produto> selectProduto();
    
    @Delete("DELETE FROM produto where id=#{id}")
    void deleteProduto(Produto produto);
    
    @Update("UPDATE produto SET nome=#{nome}, descricao=#{descricao}," +
            " valor=#{valor}, qtde=#{qtde} where id=#{id}")
    void updateProduto(Produto produto);
}
