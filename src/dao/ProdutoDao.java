/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexao.Conexao;
import java.util.ArrayList;
import mapper.ProdutoMapper;
import org.apache.ibatis.session.SqlSession;
import produto.Produto;

/**
 *
 * @author Ycaro
 */
public class ProdutoDao {
    public void save(Produto produto){
            SqlSession session = Conexao.getSqlSessionFactory().openSession();	
            ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
            mapper.insertProduto(produto);
            session.commit();
            session.close();
    }
    
    public ArrayList<Produto> listarProdutos(){
        ArrayList<Produto> produtos = new ArrayList();
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        try {
            produtos = mapper.selectProduto();
            return produtos;
        } finally {
            session.close();
        }  
    }
    
    public void deleteProduto(Produto produto){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.deleteProduto(produto);
        session.commit();
        session.close();
    }
    
    public void atualizarProduto(Produto produto){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.updateProduto(produto);
        session.commit();
        session.close();
    }
}
