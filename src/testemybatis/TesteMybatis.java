/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testemybatis;

import dao.ProdutoDao;
import java.util.ArrayList;
import produto.Produto;

/**
 *
 * @author Ycaro
 */
public class TesteMybatis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ProdutoDao produtoDao = new ProdutoDao();
        ArrayList<Produto> produtos = new ArrayList();
        
        Produto produto = new Produto();
        produto.setNome("Boca");;
//        produto.setValor(9.9);
//        produtoDao.save(produto);
        
        produtos = produtoDao.listarProdutos();
        
        produto = produtos.get(0);;
        produto.imprimir();
        produto.setNome("Ycaro");
        produtoDao.atualizarProduto(produto);
        produto.imprimir();
//        produtoDao.deleteProduto(produto);
//        for( int i=0; i < produtos.size(); i++ ){
//            produtos.get(i).imprimir();
//        }
    }
    
}
